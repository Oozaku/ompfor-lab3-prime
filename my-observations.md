# My observations about lab 3

In this lab, I tested four cases using for in OpenMP searching for the best performance: 

- parallelization with no scheduling
- parallelization with static scheduling
- parallelization with dynamic scheduling
- parallelization with guided scheduling

With small number all of those tests performed worse than with serial program. But with big numbers, using scheduling showed a performance improvement, but in static scheduling, I had to increase the chuncksize from 1 to 10 in order to get a performance close to dynamic's and guided's performance. 
