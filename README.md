# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## Prime

A prime number (or a prime) is a natural number greater than 1 that cannot be formed by multiplying two smaller natural numbers.

See [Wikipedia](https://en.wikipedia.org/wiki/Prime_number).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor. Build the application on your computer and run it.
4. Compute the execution time for computing the prime function and display it.
5. Parallelize the code using OpenMP.
6. Duplicate the prime function and experiment with the different loop scheduling strategies available in the OpenMP runtime. Display the execution time for each strategy. What do you observe?


Anything missing? Ideas for improvements? Make a pull request.
