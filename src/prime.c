#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int main(int argc, char *argv[]);
int prime_default(int n);
int prime_parall_normal(int n);
int prime_parall_static(int n);
int prime_parall_dynamic(int n);
int prime_parall_guided(int n);
void printPerformance(double t_ref, double t_spent, char *name);

	int main(int argc, char *argv[])
{
	int n;
	int n_factor;
	int n_hi;
	int n_lo;
	int primes;

	n_lo = 1;
	n_hi = 131072000;
	n_factor = 2;

	printf("\n");
	printf("                           Default \n");
	printf("         N     Pi(N)          Time \n");
	printf("\n");

	n = n_lo;
	
	double t_start, t_end, time_spent_ref, time_spent;
	while (n <= n_hi) {
		
		printf("-----------------------------------------\n");
		/* Reference values */
		t_start = omp_get_wtime();
		primes = prime_default(n);
		t_end = omp_get_wtime();
		time_spent_ref = t_end - t_start;
		printf("> prime_default:");
		printf("  %8d  %8d\n", n, primes);
		printf("Time spent = %lf\n\n", time_spent_ref);
	
		/* Parallelization with no scheduling */
		t_start = omp_get_wtime();
		primes = prime_parall_normal(n);
		t_end = omp_get_wtime();
		time_spent = t_end - t_start;
		printPerformance(time_spent_ref,time_spent,"normal");

		/* Parallelization with static scheduling */
		t_start = omp_get_wtime();
		primes = prime_parall_static(n);
		t_end = omp_get_wtime();
		time_spent = t_end - t_start;
		printPerformance(time_spent_ref,time_spent,"static");

		/* Parallelization with dynamic scheduling */
		t_start = omp_get_wtime();
		primes = prime_parall_dynamic(n);
		t_end = omp_get_wtime();
		time_spent = t_end - t_start;
		printPerformance(time_spent_ref,time_spent,"dynamic");

		/* Parallelization with guided scheduling */
		t_start = omp_get_wtime();
		primes = prime_parall_guided(n);
		t_end = omp_get_wtime();
		time_spent = t_end - t_start;
		printPerformance(time_spent_ref,time_spent,"guided");

		n = n * n_factor;
	}
	/*
	   Terminate.
	   */
	printf("\n");
	printf("  Normal end of execution.\n");

	return 0;
}


/*
Purpose:
counts primes.
Licensing:
This code is distributed under the GNU LGPL license.
Modified:
10 July 2010
Author:
John Burkardt
Parameters:
Input, the maximum number to check.
Output, the number of prime numbers up to N.
*/

int prime_default(int n)
{
	int i;
	int j;
	int prime;
	int total = 0;

	for (i = 2; i <= n; i++) {
		prime = 1;

		for (j = 2; j < i; j++) {
			if (i % j == 0) {
				prime = 0;
				break;
			}
		}
		total = total + prime;
	}
	
	return total;
}

/* Parallelization with no schedule */
int prime_parall_normal(int n)
{
	int i;
	int j;
	int prime;
	int total = 0;
# 	pragma omp parallel for reduction (+:total)
	for (i = 2; i <= n; i++) {
		prime = 1;

		for (j = 2; j < i; j++) {
			if (i % j == 0) {
				prime = 0;
				break;
			}
		}
		total += prime;
	}
	
	return total;
}
/* Parallelization with static schedule */
int prime_parall_static(int n)
{
	int i;
	int j;
	int prime;
	int total = 0;
# 	pragma omp parallel for reduction (+:total)\
	schedule(static,10)
	for (i = 2; i <= n; i++) {
		prime = 1;

		for (j = 2; j < i; j++) {
			if (i % j == 0) {
				prime = 0;
				break;
			}
		}
		total += prime;
	}
	
	return total;
}

/* Parallelization with dynamic schedule */
int prime_parall_dynamic(int n)
{
	int i;
	int j;
	int prime;
	int total = 0;
# 	pragma omp parallel for reduction (+:total)\
	schedule(dynamic,1)
	for (i = 2; i <= n; i++) {
		prime = 1;

		for (j = 2; j < i; j++) {
			if (i % j == 0) {
				prime = 0;
				break;
			}
		}
		total += prime;
	}
	
	return total;
}

/* Parallelization with guided schedule */
int prime_parall_guided(int n)
{
	int i;
	int j;
	int prime;
	int total = 0;
# 	pragma omp parallel for reduction (+:total) \
	schedule(guided)
	for (i = 2; i <= n; i++) {
		prime = 1;

		for (j = 2; j < i; j++) {
			if (i % j == 0) {
				prime = 0;
				break;
			}
		}
		total += prime;
	}
	
	return total;
}

void printPerformance(double t_ref, double t_spent, char *name){
	printf("%s performance gained = %lf\n",name,t_ref/t_spent);
	printf("----------------------------------\n");
}


